Double boson events= 4706.45
Drellyan events= 6909.72
W boson events= 3.20695e+06
Z boson events= 144615
Single top events= 5197.27
ttbar events= 18777.9
------------------
Total MC events= 3.38716e+06
Signal events= 3.20695e+06
Background events= 180207
data events= 3.56007e+06
------------------
Data/MC = 1.05105
Purity= 0.946797
Significance (for measurements)= 1742.51
Significance (for searches)= 7554.52
